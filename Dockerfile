FROM openjdk:17-jdk-alpine3.14

RUN apk add curl jq

# Workspace
WORKDIR /usr/share/gasco

# ADD .jar under target from host into this image
ADD target/gasco-test-docker-bdd-tests.jar gasco-test-docker-bdd-tests.jar
ADD target/test-classes/ test-classes/
ADD target/libs/ libs/
ADD src src/
ADD healthcheck.sh healthcheck.sh

ENTRYPOINT sh healthcheck.sh