# Test Automation Gasco - Alejandro Contreras
Este projecto es realizado para postular al cargo de Consultor Automatizador para Gasco.

## Metodología utilizada
- Como no se especificó metodología en la cual automatizar, usé BDD, y utilizé el DSL Gherkin para escribir los casos de prueba.

## Casos de prueba generados con metodología BDD y Gherkin
- Se generaron dos casos de prueba, los cuales están escritos en Gherkin y ubicados en la ruta:
    - src/test/resources/features 
    - El archivo que contiene los 2 casos de prueba tiene por nombre: gasco-test.feature.

## Herramientas utilizadas

- Java OpenJDK17, como lenguaje de programación.
- Maven 3.8.3, como herramienta de gestión de proyecto y dependencias.
- Git: como host del repo se utiliza Bitbucket.
  - `https://aleinnformatico@bitbucket.org/aleinnformatico/gasco-test.git`.
- Librerías: 
    - Cucumber java, cucumber junit, cucumbertestng para realizar pruebas.
    - Cucumber picocontainer para inyección de dependencias.
    - Selenium para automatizar Browser.
    - WebDriverManager para manjear el driver de navegadores de manera automatizada.
    - Log4j para logger.

## Cómo Ejecutarlo
- Para ejecutar de manera limpia este proyecto ejecute `mvn clean test` en la carpeta del proyecto, a nivel del fichero pom.xml (es necesario tener instalado Maven). También se le pueden agregar parámetros adicionales para ejecutar en distintos navegadores.
- Por defecto si sólo se ejecuta un `mvn clean test` se ejecutará la prueba en Chrome. 
- Pero también podemos pasar el argumento `-Dbrowser=firefox` para ejecutar en Firefox --> Ejemplo: `mvn clean test -Dbrowser=firefox`. 
- También podemos ejecutarlo en Selenium Grid levantada en una VM personal de Google Cloud Platform.
    - El enlace es el siguiente --> http://34.125.87.154:4444/ui/index.html#/
    - Para ejecutarlo en dicha instancia debemos declarar el navegador remoto usando `mvn clean test -Dbrowser=chromeDocker`
    - La herramienta ubicada en el enlace provisto tiene un pequeño VNC para poder ver cómo se ejecutan las pruebas, este visor está ubicado en la ruta:
    - http://34.125.87.154:4444/ui/index.html#/sessions  --> La clave para conectarse al VNC es `secret`

## Reportería
- Para realizar seguimiento de lo ejecutado, utilizé el reporte tipo que ofrece la herramienta Cucumber.
- Cree una carpeta en Github para guardar la trazabilidad de las ejecuciones con los respectivos reportes
- El enlace a los reportes es el siguiente: 
    - https://reports.cucumber.io/report-collections/ab37ae06-480d-459e-ae6a-c3e4aaf670ca
- Enlace a una ejecución exitosa:
    - https://reports.cucumber.io/reports/817a0f6c-385c-4395-b0a5-8ab0d4ac375e