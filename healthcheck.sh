#!/bin/bash

echo "........................................................."
echo "..........G  A  S  C  O     T  E  S  T .................."
echo "........................................................."
echo "Chequeando si el HUB está listo para ejecutar las pruebas"
echo "IP del hub es -> $HUB_HOST"
echo "........................................................."

while [ "$(curl -s http://$HUB_HOST:4444/status | jq -r .value.nodes[0].availability)" != "UP" ]
do
  sleep 3
done
# start the java command
java -cp gasco-test-docker-bdd-tests.jar:test-classes/:libs/*:src/test/resources/dataqa.properties io.cucumber.core.cli.Main -t $TAG