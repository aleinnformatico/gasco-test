package cl.gasco.gascotest.pageobjects;

import cl.gasco.gascotest.utils.BasePage;
import cl.gasco.gascotest.utils.MetodosGenericos;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import static cl.gasco.gascotest.utils.MetodosGenericos.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

/**
 * Clase Home Page que hace referencia a la HomePage de GASCO
 * @url = https://www.gasco.cl/
 * @author Alejandro Contreras
 */
public class HomePage extends BasePage {

    // Elementos WEB
    @FindBy(how = How.ID, using = "compra-tab")
    private WebElement botonGasADomicilio;

    @FindBy(how = How.ID, using = "estanque-tab")
    private WebElement botonEstanqueDeGas;

    @FindBy(how = How.ID, using = "rapidito-tab")
    private WebElement botonPagaEnLinea;

    @FindBy(how = How.ID, using = "sucursal-tab")
    private WebElement botonSucursalVirtual;

    @FindBy(how = How.XPATH, using = "//*[@class=\"btn-form-entrar pedido2020\"]")
    private WebElement botonHacerPedido;

    // Métodos
    public void abroPaginaWeb(String url){
        driver.get(url);
        log.debug("Ingresando a pagina web: " + url);

        log.debug("Verificando que pagina web cargue correctamente");
        Assert.assertEquals(driver.getCurrentUrl(), url);
        wait.until(titleIs("Gas para el Hogar a Domicilio en Chile | Gasco"));
    }

    public void apretoBotonGasADomicilio() {
        log.debug("Esperando que boton de Gas a Domicilio sea visible y cliqueable");
        waitForElementToBeVisibleAndClickable(this.botonGasADomicilio, this.wait);

        log.debug("Apretando boton Gas a Domicilio");
        this.botonGasADomicilio.click();
        log.info("Se realiza clik a tab Gas a Domicilio");
    }

    public void apretoBotonHacerPedido() {
        log.debug("Esperando que boton Hacer Pedido sea visible y cliqueable");
        waitForElementToBeVisibleAndClickable(this.botonHacerPedido, this.wait);

        //Manejo de ventanas
        String originalWindow = driver.getWindowHandle();
        log.debug("El id de la ventana actual es: " + originalWindow);
        assert driver.getWindowHandles().size() == 1;
        log.debug("Chequeando de que solo exista 1 ventana abierta");
        //Cliqueo botón que abre nueva ventana
        log.debug("Se realizara click en boton Hacer Pedido");
        this.botonHacerPedido.click();
        log.info("Se realiza click en boton Hacer Pedido");

        //Espero que carge nueva ventana y verifico que exista
        wait.until(numberOfWindowsToBe(2));
        log.debug("Espero que el numero de ventanas ahora sea de 2");

        var tabs = driver.getWindowHandles();
        if (tabs.size() > 1) {
            driver.switchTo().window(tabs.stream().toList().get(1));
            log.debug("Cambiando a la segunda pestana: " +  tabs.stream().toList().get(1) +
                    " desde la ventana: " + originalWindow);

            //Espero que la nueva página carge correctamente
            log.debug("Esperando que el nuevo tab carge correctamente");
            wait.until(titleIs("Energía limpia | Gasco"));
            wait.until(urlToBe("https://pedidos.gasco.cl/#/cilindro/pedido-cilindro"));
        } else {
            log.fatal("No se pudo cambiar el contexto a la nueva pestana");
        }
    }

}
