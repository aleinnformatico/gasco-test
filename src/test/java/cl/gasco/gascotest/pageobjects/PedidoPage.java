package cl.gasco.gascotest.pageobjects;

import cl.gasco.gascotest.utils.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import java.util.List;

import static cl.gasco.gascotest.utils.MetodosGenericos.*;

/**
 * Clase PedidoPage que hace referencia a la HomePage de GASCO
 * @url = https://pedidos.gasco.cl/#/cilindro/pedido-cilindro
 * @author Alejandro Contreras
 */
public class PedidoPage extends BasePage {

    // Elementos WEB
    @FindBy(how = How.ID, using = "mat-input-0")
    private WebElement fieldTelefono;

    @FindBy(how = How.XPATH, using = "//*[@class=\"gascoButtonContainer_primary\"]")
    private WebElement btnContinuar;

    @FindBy(how = How.XPATH, using = "//*[@class=\"gascoButtonContainer_primary_outline conIconoLeft\"]")
    private WebElement btnAnadirDirecion;

    @FindBy(how = How.ID, using = "noInputGoogle")
    private WebElement fieldDireccion;

    @FindBy(how = How.ID, using = "inputGoogle")
    private WebElement fieldDireccionGoogle;

    @FindBy(how = How.ID, using = "mat-input-1")
    private WebElement fieldDeptoCasaBloque;

    @FindBy(how = How.ID, using = "mat-input-2")
    private WebElement fieldComentarios;

    @FindBy(how = How.CSS, using = "span.pasoPedido1__grupo__tarjeta__radio__texto")
    private WebElement txtDireccionCreada;

    @FindBy(how = How.ID, using = "mat-radio-2")
    private WebElement radioButtonDireccion;

    @FindBy(how = How.XPATH, using = "//button[@class=\"mat-ripple mInstruccion__contenido__accion\"]")
    private WebElement btnContinuarMensajeModal;

    @FindBy(how = How.ID, using = "cdk-overlay-0")
    private WebElement panelInstruccionesMensajeModal;

    @FindBy(how = How.XPATH, using = "//*[@class=\"pasoPedido2\"]")
    private WebElement contenedorPasoPedido2;

    @FindBy(how = How.XPATH,
            using = "//button[@class=\"mat-ripple pasoPedido2__opciones__normal elemento--botonActivo\"]")
    private WebElement tabCilindroNormal;

    @FindBy(how = How.XPATH, using = "//button[@class=\"mat-ripple pasoPedido2__opciones__catalitico\"]")
    private WebElement tabCilindroCatalitico;

    @FindBy(how = How.XPATH,
            using = "//button[@class=\"mat-ripple pasoPedido2__contenido__cilindros__cilindro__contador__sumar\"]")
    // Ya que la clase hace referencia a todos los botones que suman cilindros, se crea una lista de Webelements
    // con todas las opciones
    private List<WebElement> btnAgregarCilindros;

    @FindBy(how = How.ID, using = "pedido-cilindro-form-normal")
    private WebElement contendorCilindrosNormales;

    @FindBy(how = How.ID, using = "pedido-cilindro-form-catalitico")
    private WebElement contendorCilindrosCataliticos;

    @FindBy(how = How.XPATH, using = "//*[@class=\"gascoButtonContainer_primary\"]")
    private WebElement btnContinuarHaciaDatosPersonales;

    @FindBy(how = How.ID, using = "mat-input-1")
    private WebElement campoNombre;

    @FindBy(how = How.ID, using = "mat-input-2")
    private WebElement campoCorreoElectronico;

    @FindBy(how = How.ID, using = "mat-input-3")
    private WebElement campoRut;

    @FindBy(how = How.XPATH, using = "//*[@class=\"pasoPedido3\"]")
    private WebElement contenedorPasoPedido3;

    @FindBy(how = How.ID, using = "mat-select-0")
    private WebElement comboboxMedioDePago;

    @FindBy(how = How.XPATH, using = "//*[@class=\"gascoButtonContainer_primary\"]")
    private WebElement btnContinuarPedidoFinal;

    @FindBy(how = How.ID, using = "pago-cuenta-form4")
    private WebElement contenedorResumenDelPedido;

    @FindBy(how = How.XPATH, using = "//*[@class=\"pasoPedido4__resumen__contenido__detalles__item__info__titulo\"]")
    private List<WebElement> resumenCantidadDeItems;

    @FindBy(how = How.XPATH, using = "//*[@class=\"pasoPedido4__contenido-detalle\"]")
    private List<WebElement> datosResumen;

    // Métodos
    public void ingresoTelefono(String telefono){
        log.debug("Esperando que el campo telefono este visible y cliqueable");
        waitForElementToBeVisibleAndClickable(this.fieldTelefono, this.wait);

        this.fieldTelefono.click();
        log.debug("Realizando click en campo Telefono");

        this.fieldTelefono.sendKeys(telefono);
        log.info("Se ingresa el telefono: " + telefono);
    }

    public void apretoBotonContinuarADireccion(){
        esperoQueElementoDeCargaDesaparezca("//*[@role=\"progressbar\"]", this.driver);

        log.debug("Esperando que el boton continuar este cliqueable");
        waitForElementToBeVisibleAndClickable(this.btnContinuar, this.wait);

        this.btnContinuar.click();
        log.info("Realizo click en boton continuar hacia Direccion");
    }

    public void apretoBotonAnadirNuevaDireccion(){
        esperoQueElementoDeCargaDesaparezca("//*[@role=\"progressbar\"]", this.driver);

        log.debug("Esperando que el boton Anadir nueva direccion este visible y cliqueable");
        waitForElementToBeVisibleAndClickable(this.btnAnadirDirecion, this.wait);

        this.btnAnadirDirecion.click();
        log.info("Realizo click en boton Anadir Direccion");
    }

    public void agregoNuevaDireccion(String direccion, String depto, String comentarios){
        log.debug("Espero que campos para anadir direccion esten cargados y cliqueables");
        waitForElementToBeVisibleAndClickable(this.fieldDireccion, this.wait);
        waitForElementToBeVisibleAndClickable(this.fieldDeptoCasaBloque, this.wait);
        waitForElementToBeVisibleAndClickable(this.fieldComentarios, this.wait);

        log.debug("Ingresando datos de nueva direccion");
        this.fieldDireccion.sendKeys(direccion + Keys.ENTER);
        // Todo verificar con un rest assured carga del servicio de AutocompletionService
        esperar(2);

        log.debug("Esperando que carge servicio de Google Maps y cambia el nombre del campo");
        this.fieldDireccionGoogle.click();
        this.fieldDireccionGoogle.sendKeys(Keys.SPACE);
        this.fieldDireccionGoogle.sendKeys(Keys.ARROW_DOWN);
        this.fieldDireccionGoogle.sendKeys(Keys.ENTER);
        log.info("Ingresando direccion: " + direccion);
        esperar(2);

        this.fieldDeptoCasaBloque.click();
        this.fieldDeptoCasaBloque.sendKeys(depto);
        log.info("Ingresando depto: " + depto);
        esperar(2);

        this.fieldComentarios.click();
        this.fieldComentarios.sendKeys(comentarios);
        log.info("Ingresando comentarios: " + comentarios);
        esperar(2);
    }

    public void apretoBotonGuardarDireccion(){
        log.debug("Presionando botón guardar direccion");
        this.fieldComentarios.submit();
        log.info("Presiono boton guardar direccion");
    }

    public void verificoDireccionCreada(String direccion, String depto, String comentarios){
        esperoQueElementoDeCargaDesaparezca("//*[@role=\"progressbar\"]", this.driver);

        log.debug("Esperando que el texto de la direccion creada sea visible");
        waitForElementToBeVisible(this.txtDireccionCreada, this.wait);

        String direccionPorFront = this.txtDireccionCreada.getText().trim();
        log.info("La direccion creada y que se ve por front es: " + direccionPorFront);

        log.debug("Verificando que direccion que se ve por front contenga a la entregada por parametros");
        String direccionPorProperties = direccion + ", " + depto + ". " + comentarios + ".";
        Assert.assertEquals(direccionPorProperties, direccionPorFront);
    }

    public void cierroVentanaOPestana(){
        log.debug("Se cerrara ventana actual y se centrara el foco en la primera pestana");
        String originalWindow = driver.getWindowHandle(); // --> Guardo ID de la ventana actual
        log.debug("El id de la ventana actual es: " + originalWindow);

        var tabs = driver.getWindowHandles(); // --> Obtengo ventanas
        if (tabs.size() > 1) {
            driver.switchTo().window(tabs.stream().toList().get(1));
            log.debug("Cerrando la segunda pestana: " +  tabs.stream().toList().get(1));
            driver.close();
            driver.switchTo().window(tabs.stream().toList().get(0));
            log.debug("Cambiando a la primera pestana: " +  tabs.stream().toList().get(0));
        }
    }

    public void cierroNavegador() {
        //this.driver.quit(); // --> No necesario ya que clase Hook,
        // en el after, al terminar la prueba cierra el navegador
    }

    public void seleccionoDireccionDeEnvio() {
        esperoQueElementoDeCargaDesaparezca("//*[@role=\"progressbar\"]", this.driver);

        log.debug("Esperando que radio button de direccion este visible y cliqueable");
        waitForElementToBeVisibleAndClickable(this.radioButtonDireccion, this.wait);
        this.radioButtonDireccion.click();
        log.info("realizo click en radiobutton de direccion");
    }

    public void apretoBtnContinuarAlPedido() {
        log.debug("Se apretara boton continuar hacia seleccion de cilindros");
        this.radioButtonDireccion.submit();
        log.info("Se apreta boton continuar a seleccion de cilindros");
    }

    public void agregoProductosAlCarro(){
        log.debug("Validando si carga o no el mensaje modal");
        if(isElementExist(this.driver, By.xpath("//button[@class=\"mat-ripple mInstruccion__contenido__accion\"]"))){
            log.debug("Espero que boton continuar del mensaje modal este visible y cliqueable");
            waitForElementToBeVisible(this.panelInstruccionesMensajeModal, this.wait);
            waitForElementToBeVisibleAndClickable(this.btnContinuarMensajeModal, this.wait);
            this.btnContinuarMensajeModal.click();
            log.info("Apreto boton continuar en mensaje modal");
        }
        log.debug("Espero que contenedor de paso pedido 2 este visible");
        waitForElementToBeVisible(this.contenedorPasoPedido2, this.wait);

        agregoCilindroNormal();
        agregoCilindroCatalitico();
    }

    public void agregoCilindroNormal(){
        log.debug("Espero que boton de seleccion cilindros normales este visible y cliqueable");
        waitForElementToBeVisibleAndClickable(this.tabCilindroNormal, this.wait);
        this.tabCilindroNormal.click();
        log.info("Realizo click en Tab de cilindros normales");
        esperoQueElementoDeCargaDesaparezca("//div[@class=\"mat-ripple-element\"]", this.driver);

        log.debug("Espero que contenedor de cilindros normales este visible");
        waitForElementToBeVisible(this.contendorCilindrosNormales, this.wait);

        this.btnAgregarCilindros.get(0).click();
        log.info("Se agrega un cilindro normal");
    }

    public void agregoCilindroCatalitico(){
        log.debug("Espero que boton de seleccion cilindros cataliticos este visible y cliqueable");
        waitForElementToBeVisibleAndClickable(this.tabCilindroCatalitico, this.wait);
        this.tabCilindroCatalitico.click();
        log.info("Realizo click en Tab de cilindros cataliticos");
        esperoQueElementoDeCargaDesaparezca("//div[@class=\"mat-ripple-element\"]", this.driver);

        log.debug("Espero que contenedor de cilindros cataliticos este visible");
        waitForElementToBeVisible(this.contendorCilindrosCataliticos, this.wait);

        this.btnAgregarCilindros.get(0).click();
        log.info("Se agrega un cilindro catalitico");
    }

    public void apretoBtnContinuarHaciaDatosDeContacto() {
        log.debug("espero que boton continuar este visible y cliqueable");
        waitForElementToBeVisibleAndClickable(this.btnContinuarHaciaDatosPersonales, this.wait);
        this.btnContinuarHaciaDatosPersonales.submit();
    }

    public void ingresoDatosDeUsuario(String nombre, String correo, String rut) {
        log.debug("Espero que elementos con los que se va a interactuar esten visibles");
        waitForElementToBeVisible(this.contenedorPasoPedido3, this.wait);
        waitForElementToBeVisibleAndClickable(this.campoNombre, this.wait);
        waitForElementToBeVisibleAndClickable(this.campoCorreoElectronico, this.wait);
        waitForElementToBeVisibleAndClickable(this.campoRut, this.wait);

        this.campoNombre.clear();
        this.campoNombre.sendKeys(nombre);
        log.info("se ingresa nombre: " + nombre);
        esperar(1);

        this.campoCorreoElectronico.clear();
        this.campoCorreoElectronico.sendKeys(correo);
        log.info("se ingresa correo: " + correo);
        esperar(1);

        this.campoRut.clear();
        this.campoRut.sendKeys(rut);
        log.info("se ingresa rut: " + rut);
        esperar(1);
    }

    public void seleccionoFormaYMedioDePago(String medioDePago){
        waitForElementToBeVisibleAndClickable(this.comboboxMedioDePago, this.wait);
        this.comboboxMedioDePago.click();
        driver.findElement(
                By.xpath("//span[@class=\"mat-option-text\" and contains(text(),'" + medioDePago +"')]")).click();
        log.info("Se selecciona medio de pago: " + medioDePago);
        esperar(1);
    }

    public void apretoBtnContinuarHaciaPasoFinal(){
        log.debug("Esperando que btn continuar hacia paso final este visible y cliqueable");
        waitForElementToBeVisibleAndClickable(this.btnContinuarPedidoFinal, this.wait);
        this.btnContinuarPedidoFinal.submit();
        log.info("Hago click en boton continuar a Paso final");
    }

    public void verificoYCapturoResumenDelPedido(int cantidad, String telefono,
                                                 String direccion, String depto,
                                                 String comentario, String medioDePago){
        log.debug("Esperando que datos resumen del pedido esten cargados correctamente");
        waitForElementToBeVisible(this.contenedorResumenDelPedido, this.wait);

        Assert.assertEquals(this.resumenCantidadDeItems.size(), cantidad);
        Assert.assertEquals(this.datosResumen.get(0).getText().trim(), telefono);
        String direccionPorProperties = direccion + ", " + depto + ". " + comentario + ".";
        Assert.assertEquals(this.datosResumen.get(1).getText().trim(), direccionPorProperties);
        Assert.assertEquals(this.datosResumen.get(2).getText().trim(), comentario);
        Assert.assertEquals(this.datosResumen.get(3).getText().trim(), medioDePago);
    }
}
