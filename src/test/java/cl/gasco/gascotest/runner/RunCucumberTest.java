package cl.gasco.gascotest.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features"},
        glue = {"cl/gasco/gascotest/stepdefinitions"},
        tags = "@gasco-test",
        plugin = {"summary","pretty",
                "json:target/cucumber/report.json",
                "html:target/cucumber/report.html",
                "testng:target/cucumber/report.xml"},
        monochrome=false,
        dryRun=false
)

public class RunCucumberTest {
}