package cl.gasco.gascotest.stepdefinitions;

import cl.gasco.gascotest.pageobjects.HomePage;
import cl.gasco.gascotest.pageobjects.PedidoPage;
import cl.gasco.gascotest.utils.MetodosGenericos;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;
import java.util.Properties;

public class StepDefinitions {

    Properties prop = MetodosGenericos.determinarPropertiesPorAmbiente("src/test/resources/");

    HomePage homePage;
    PedidoPage pedidoPage;

    public StepDefinitions(HomePage homePage, PedidoPage pedidoPage) throws IOException {
        this.homePage = homePage;
        this.pedidoPage = pedidoPage;
    }

    @Given("Ingreso a pagina de Gasco")
    public void ingreso_a_pagina_de_gasco() {
        homePage.abroPaginaWeb(
                this.prop.getProperty("url")
        );
    }

    @And("selecciono opcion Gas a Domicilio y apreto boton hacer pedido")
    public void seleccionoOpcionGasADomicilioYApretoBotonHacerPedido() {
        homePage.apretoBotonGasADomicilio();
        homePage.apretoBotonHacerPedido();
    }

    @When("ingreso telefono y apreto boton continuar")
    public void ingreso_telefono_y_apreto_boton_continuar() {
        pedidoPage.ingresoTelefono(this.prop.getProperty("telefono"));
        pedidoPage.apretoBotonContinuarADireccion();
    }

    @And("anado nueva direccion y apreto boton guardar")
    public void anadoNuevaDireccionYApretoBotonGuardar() {
        pedidoPage.apretoBotonAnadirNuevaDireccion();
        pedidoPage.agregoNuevaDireccion(
                this.prop.getProperty("direccion"),
                this.prop.getProperty("depto"),
                this.prop.getProperty("comentarios"));
        pedidoPage.apretoBotonGuardarDireccion();
    }

    @Then("capturo y verifico direccion registrada")
    public void capturo_y_verifico_direccion_registrada() {
        pedidoPage.verificoDireccionCreada(
                this.prop.getProperty("direccion"),
                this.prop.getProperty("depto"),
                this.prop.getProperty("comentarios"));
    }

    @Then("cierro ventana")
    public void cierro_ventana() {
        pedidoPage.cierroVentanaOPestana();
        pedidoPage.cierroNavegador();
    }

    @Given("ingreso telefono registrado y apreto boton continuar")
    public void ingreso_telefono_registrado_y_apreto_boton_continuar() {
        pedidoPage.ingresoTelefono(this.prop.getProperty("telefono"));
        pedidoPage.apretoBotonContinuarADireccion();
    }

    @Given("compruebo direccion de envio registrada")
    public void compruebo_direccion_de_envio_registrada() {
        pedidoPage.verificoDireccionCreada(
                this.prop.getProperty("direccion"),
                this.prop.getProperty("depto"),
                this.prop.getProperty("comentarios"));
    }

    @When("apreto boton continuar hacia la realizacion del pedido")
    public void apreto_boton_continuar_hacia_la_realizacion_del_pedido() {
        pedidoPage.seleccionoDireccionDeEnvio();
        pedidoPage.apretoBtnContinuarAlPedido();
    }

    @And("selecciono un cilindo normal y un catalitico")
    public void seleccionoUnCilindoNormalYUnCatalitico() {
        pedidoPage.agregoProductosAlCarro();
    }

    @When("apreto boton continuar hacia los datos de contacto")
    public void apreto_boton_continuar_hacia_los_datos_de_contacto() {
        pedidoPage.apretoBtnContinuarHaciaDatosDeContacto();
    }

    @When("ingreso datos de contacto")
    public void ingreso_datos_de_contacto() {
        pedidoPage.ingresoDatosDeUsuario(
                prop.getProperty("nombre"),
                prop.getProperty("correo"),
                prop.getProperty("rut"));
    }

    @When("selecciono la forma y el medio de pago")
    public void selecciono_la_forma_y_el_medio_de_pago() {
        pedidoPage.seleccionoFormaYMedioDePago(prop.getProperty("medioDePago"));
    }

    @When("apreto boton continuar hacia la confirmacion de compra")
    public void apreto_boton_continuar_hacia_la_confirmacion_de_compra() {
        pedidoPage.apretoBtnContinuarHaciaPasoFinal();
    }

    @Then("compruebo datos del resumen del pedido")
    public void compruebo_datos_del_resumen_del_pedido() {
        pedidoPage.verificoYCapturoResumenDelPedido(2,
                this.prop.getProperty("telefono"),
                this.prop.getProperty("direccion"),
                this.prop.getProperty("depto"),
                this.prop.getProperty("comentarios"),
                this.prop.getProperty("medioDePago"));
    }
}
