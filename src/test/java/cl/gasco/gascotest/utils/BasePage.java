package cl.gasco.gascotest.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected static final Logger log = LogManager.getLogger();

    private static final int TIMEOUT = 10;
    private static final int POLLING = 10;

    /**
     * Constructor de clase en el cual se setea un wait de elementos, el WebDriver
     * e inicializa los elementos de una clase Page usando initElements de PageFactory
     */
    public BasePage(){
        this.driver = WebDriverFactory.getDriver();
        wait = new WebDriverWait(this.driver, Duration.ofSeconds(TIMEOUT), Duration.ofMillis(POLLING));
        PageFactory.initElements(new AjaxElementLocatorFactory(WebDriverFactory.getDriver(), TIMEOUT), this);
    }
}

//        PageFactory.initElements(WebDriverFactory.getDriver(), this);
