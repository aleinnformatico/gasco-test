package cl.gasco.gascotest.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

public class MetodosGenericos {

    protected static final Logger log = LogManager.getLogger();

    public static Properties readPropertiesFile(String fileName) throws IOException {
        FileInputStream fis = null;
        Properties prop = null;
        try {
            fis = new FileInputStream(fileName);
            prop = new Properties();
            prop.load(fis);
        } catch(IOException fnfe) {
            fnfe.printStackTrace();
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
        return prop;
    }

    /**
     * Método que permite determinar el archivo properties con datos segun el ambiente determinado en la propiedad de sistema 'env',
     * los archivos pueden ser 'dataqa.properties' o 'dataprod.properties'
     * @param rutaDeProperties Path o ruta donde se encuentran los archivo properties, si esta en la raiz del proyecto, no debe entregarse argumento.
     *                         La ruta tradicional es: 'src/test/resources/'
     */
    public static Properties determinarPropertiesPorAmbiente(String ...rutaDeProperties) throws IOException {
        String env = System.getProperty("env", "qa");
        if(rutaDeProperties.length == 0){
            rutaDeProperties = new String[]{""};
        }
        else{
            rutaDeProperties[0] = rutaDeProperties[0].trim();
            if (!rutaDeProperties[0].substring(rutaDeProperties[0].length() - 1).equals("/") && !rutaDeProperties[0].substring(rutaDeProperties[0].length() - 1).equals("\\")){
                rutaDeProperties[0] += "/";
            };
        }
        return switch (env) {
            case "qa" -> readPropertiesFile(rutaDeProperties[0] + "dataqa.properties");
            case "prod" -> readPropertiesFile(rutaDeProperties[0] + "dataprod.properties");
            default -> throw new IOException ("No existe el ambiente: '" + env +"'");
        };
    }

    /**
     * Método que permite fija posición y fijar un dropdown
     * @param element WebElement donde está el dropdown
     */
    public static void positionDropDown(WebElement element, WebDriver driver){
        Actions act = new Actions(driver);
        act.moveToElement(element).perform();
        act.moveToElement(element).click().perform();
    }

    public static void actionClick(WebElement element, WebDriver driver){
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().perform();
    }

    public static void esperoQueElementoDeCargaDesaparezca(String xpath, WebDriver driver){
        int timeout = 0;
        boolean run = true;

        while (run) {
            try {
                WebElement elemento;

                esperar(1);
                log.debug("esperando que spiner de carga desaparezca");

                elemento = new WebDriverWait(driver, Duration.ofSeconds(1))
                        .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));

                if (!elemento.isDisplayed()) {
                    run = false;
                }
            } catch (NoSuchElementException | TimeoutException | StaleElementReferenceException e) {
                run = false;
            } finally {
                timeout++;
            }
        }
    }

    /**
     * Verifica si el elemento existe
     * @param driver Recibe el driver que se está ejecutando en ese momento
     * @param by El localizador de objeto que queremos buscar
     * @return True si encuentra el objeto o False si no lo encuentra
     */
    public static boolean isElementExist(WebDriver driver, By by) {
        return driver.findElements(by).size() > 0;
    }

    public static void waitForElementToBeVisible(By by, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public static void waitForElementToBeVisible(By by, int timeout, WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(timeout));
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (Exception ignored) {}
    }

    public static WebElement waitForElementToBeVisible(WebElement element, WebDriverWait wait) {
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForElementToBeClickable(WebElement element, WebDriverWait wait) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitForElementToBeVisibleAndClickable(WebElement element, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(element));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static Boolean waitForElementNotToBeVisible(By by, WebDriverWait wait) {
        return wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public static void waitForElementToBeSelected(WebElement element, WebDriverWait wait) {
        wait.until(ExpectedConditions.elementSelectionStateToBe(element, true));
    }

    public static void waitForElementToBeRefreshed(WebElement element, WebDriverWait wait) {
        wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));
    }

    public static void waitForElementToBeRefreshed(By by, WebDriverWait wait) {
        wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(by)));
    }

    public static void waitForElementToBeClickable(By by, WebDriverWait wait) {
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public static WebElement waitForPresenceOfElement(By by, WebDriverWait wait) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public static void waitForPresenceOfAllElements(By by, WebDriverWait wait) {
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    public static boolean waitForTextToBePresentInElement(By by, String text, WebDriverWait wait) {
        try {
            if (wait.until(ExpectedConditions.textToBePresentInElementLocated(by, text)))
                return true;
        } catch (TimeoutException e) {
            return false;
        }
        return false;
    }

    public static boolean waitForTextToBeNonEmpty(final By by, WebDriver driver) {
        (new WebDriverWait(driver, Duration.ofSeconds(30))).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(by).getText().length() != 0;
            }
        });
        return false;
    }

    public static void waitForInvisibilityOfElementByText(By by, String text, WebDriverWait wait) {
        wait.until(ExpectedConditions.invisibilityOfElementWithText(by, text));
    }

    public static void waitForElementToBeInvisible(By by, WebDriverWait wait) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public static void waitForElementToBeChecked(WebElement element, WebDriverWait wait) {
        wait.until(elementToBeChecked(element));
    }

    private static ExpectedCondition<WebElement> elementToBeChecked(
            final WebElement element) {
        return new ExpectedCondition<WebElement>() {

            public final ExpectedCondition<WebElement> visibilityOfElement =
                    ExpectedConditions.visibilityOf(element);

            @Override
            public WebElement apply(WebDriver driver) {
                WebElement element = visibilityOfElement.apply(driver);
                try {
                    if (element != null && element.getAttribute("checked").equals("true")) {
                        return element;
                    } else {
                        return null;
                    }
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return "element to be checked : " + element;
            }
        };
    }

    public static void esperar(int segundos){
        try {
            Thread.sleep(segundos * 1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
