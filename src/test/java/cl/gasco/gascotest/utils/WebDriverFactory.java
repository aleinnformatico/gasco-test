package cl.gasco.gascotest.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Clase Utilitaria que setea los drivers a utilizar para el Browser deseado según el navegador definido
 * en la propiedad de sistema "browser".
 * Customiza el driver a través de un switch y permite seleccionar propiedades
 * para cada navegador.
 * Por defecto el browser que se abrirá será chrome.
 */
public class WebDriverFactory {
    public static ThreadLocal<WebDriver> tlDriver = new ThreadLocal<>();

    /**
     * Crea un driver específico al navegador definido en la propiedad de sistema "browser".
     * Posteriormente devuelve el driver específico al Thread desde donde se llama.
     * @param nombreTest (Opcional) Nombre del Test o Escenario para determinar el nombre en Zalenium y Docker
     * @return Devuelve un WebDriver específico para un Thread.
     */
    public WebDriver createWebDriver(String ...nombreTest) {
        String testName = nombreTest.length > 0 ? nombreTest[0] : "";

        //Se obtiene la propiedad browser y se define como por defecto "chrome" si no esta definida
        String webdriver = System.getProperty("browser", "chrome");

        switch(webdriver) {

            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions fo = new FirefoxOptions();

                ProfilesIni profileIni = new ProfilesIni();
                FirefoxProfile profile = profileIni.getProfile("default");
                profile.setAcceptUntrustedCertificates(true);
                profile.setPreference("browser.safebrowsing.enabled", false);
                profile.setPreference("extensions.blocklist.enabled", false);
                profile.setPreference("browser.tabs.warnOnClose", false);
                profile.setPreference("browser.tabs.warnOnOpen", false);
                profile.setPreference("browser.safebrowsing.malware.enabled", false);
                profile.setPreference("webdriver_accept_untrusted_certs", true);
                profile.setPreference("webdriver_enable_native_events", true);
                profile.setPreference("webdriver_assume_untrusted_issuer", true);

                fo.addArguments("-private");
                fo.setProfile(profile);

                tlDriver.set(new FirefoxDriver(fo));
                return getDriver();

            case "chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions co = new ChromeOptions();
                co.addArguments("--incognito");
                co.addArguments("--disable-gpu");
                co.addArguments("--start-maximized");
                co.addArguments("--no-sandbox");
                co.addArguments("--ignore-certificate-errors");
                co.addArguments("--disable-popup-blocking");
                co.addArguments("--window-size=1920,1080");
                co.addArguments("--disable-dev-shm-usage");
                co.addArguments("--lang=es");

                tlDriver.set(new ChromeDriver(co));
                return getDriver();

            case "chromeDocker":
                WebDriverManager.chromedriver().setup();
                ChromeOptions cop = new ChromeOptions();
                cop.addArguments("--start-maximized");

                try {
                    tlDriver.set(new RemoteWebDriver(new URL("http://34.125.87.154:4444"), cop));
                    return getDriver();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

            case "ie":
                WebDriverManager.iedriver().setup();
                InternetExplorerOptions ieo = new InternetExplorerOptions();
                tlDriver.set(new InternetExplorerDriver(ieo));
                return getDriver();

            case "safari":
                WebDriverManager.safaridriver().setup();
                tlDriver.set(new SafariDriver());
                return getDriver();

            default:
                throw new RuntimeException("Webdriver no soportado: " + webdriver);
        }
    }

    /**
     * Método para obtener el driver creado con el método createWebDriver()
     * @return Retorna el driver específico según el Thread que creo el Driver.
     */
    public static  WebDriver getDriver() {
        return tlDriver.get();
    }

}
