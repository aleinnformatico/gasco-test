@gasco-test
Feature: Compras en Gasco

  # Pasos que se repiten en ambos escenarios de prueba
  Background:
    Given Ingreso a pagina de Gasco
    And selecciono opcion Gas a Domicilio y apreto boton hacer pedido

  # Test 1
  @gasco-test @gasco-test-1
  Scenario: Test 1 -  Creacion de direccion para pedidos en pagina de GASCO
    When ingreso telefono y apreto boton continuar
    And anado nueva direccion y apreto boton guardar
    Then capturo y verifico direccion registrada
    And cierro ventana

  # Test 2
  @gasco-test @gasco-test-2
  Scenario: Test 2 - Creacion de un pedido en pagina de GASCO
    And ingreso telefono registrado y apreto boton continuar
    And compruebo direccion de envio registrada
    When apreto boton continuar hacia la realizacion del pedido
    And selecciono un cilindo normal y un catalitico
    And apreto boton continuar hacia los datos de contacto
    And ingreso datos de contacto
    And selecciono la forma y el medio de pago
    And apreto boton continuar hacia la confirmacion de compra
    Then compruebo datos del resumen del pedido